.PHONY: default
default: ;

# Format all .cpp and .h files according to the style specific in .clang-format
# This uses the clang-format tool (https://clang.llvm.org/docs/ClangFormat.html)
.PHONY: format
format:
	clang-format -i $(shell find . -name *.cpp -o -name *.h)
