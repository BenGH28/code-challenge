# code-challenge

A set of coding challenges for learning/practicing programming.

Each challenge has it's own folder containing:

* `README.md` describing the challenge and any additional resources.
* `tests` subfolder containing some test cases that a complete solution will satisfy.
* `solns` subfolder containing one or more implementations that pass the tests. Dont peak!

